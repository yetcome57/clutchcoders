package LC144;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

/**
 * Created by lipingzhang on 3/31/17.
 */
public class Solution2 {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> ans = new ArrayList<>();
        Stack<TreeNode> stack = new Stack<>();

        while(root != null || !stack.isEmpty()){
            if(root != null){
                ans.add(root.val);
                if (root.right != null) {
                    stack.push(root.right);
                }
                root = root.left;
            }else{
                root = stack.pop();
            }
        }
        return ans;
    }
}
