class Solution(object):
    def intersection(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        common = [] ## list to hold the common elements
        for num in nums1:
            if (num not in common) and (num in nums2):
                common.append(num) 
        return common
