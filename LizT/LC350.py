class Solution(object):
    def intersect(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        intersection=[]
        for num in nums1:
            if num in nums2:
                intersection.append(nums2.pop(nums2.index(num)))
                    ###find the index of the num in nums2,and remove it from nums2 using pop. The popped element is added to the intersection list.
        return intersection
